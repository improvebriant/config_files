from urllib.request import urlopen
import json
import argparse
import pprint


SERVER = 'https://improve.aqrc.ucdavis.edu'


def get_samplers():
    '''
    Gets the list of samplers from the webserver.
    Returns a list of dictionaries:
    [{'Name': site1, 'Hostname': hostname1}, {'Name': site2, 'Hostname': hostname2}, ...]
    '''
    url = SERVER + '/Samplers/ListSamplersJson'
    # pull down the data from the server (as text)
    webpage = urlopen(url)
    webpage_text = webpage.read().decode('utf-8')
    # convert into a list of samplers
    samplers = json.loads(webpage_text)
    #pprint.pprint(samplers)
    print('received', len(samplers), 'samplers from', url)
    return samplers

def update_config_file(sampler_name):
    url = SERVER + '/Samplers/ControllerConfigFile?name=' + sampler_name
    # pull down the data from the server (as text)
    #print(url)
    webpage = urlopen(url)
    config_text = webpage.read().decode('utf-8')
    filename = sampler_name + '.ini'
    with open(filename, 'w') as file_handle:
        file_handle.write(config_text)
        file_handle.close()
    print('updated', filename)


parser = argparse.ArgumentParser(description='Download updated config.ini files from server')
parser.add_argument('site', action='store', default='all', help='"all" for all sites or site name "SITEN"', nargs='?')
args = parser.parse_args()
print('site:', args.site)

if args.site.lower() == 'all':
    samplers = get_samplers()
    for sampler in samplers:
        sampler_name = sampler['Name']
        update_config_file(sampler_name)
else:
    update_config_file(args.site)

